import { _server } from "../config/server.config";
import { secrete } from "./config/jwt.config";
import { GenerateToken } from "./generate-token/generate-token";
import { route } from "./routes/jwt.routes";
import { dbJwt } from "./database/jwt.db";

const JWT = {
    pkg: {
        name: 'jwt-auth',
        version: '1'
    },
    register: async (server) => {
        try {
            const validate = async (decoded, request) => {
                try {
                    const {
                        phone,
                        email,
                        password
                    }=decoded;
                    const user=await request.server.methods.dbCustomers.findOneAndUpdate(
                        {
                            phone:phone,
                            blocked:false,
                            password:password
                        },
                        {
                            $set: {
                                last_seen:Date.now()
                            }
                        }
                    );
                    if (!user || !user._id){
                        const admin=await request.server.methods.dbAdmin.findOne(
                            {
                                email:email,
                                password:password
                            }
                        );
                        if(!admin || !admin._id){
                            throw Error(
                                'Account not found!'
                            )
                        }
                    }
                    return {
                        isValid:true
                    }
                } catch (err) {
                    return { 
                        isValid:false 
                    }; 
                }
            };

            await server.register(require('hapi-auth-jwt2'));
            server.auth.strategy('jwt', 'jwt',
                { 
                    key:secrete, // Never Share your secret key
                    validate:validate, // validate function defined above
                    verifyOptions: { algorithms: [ 'HS256' ] } // pick a strong algorithm
                }
            );

            server.method('generateJWT',GenerateToken);
            server.method('dbJwt',dbJwt);
            server.route(route);
            server.auth.default('jwt');

        } catch (err) {
            server.methods.dbErrors.create(
                {
                    id:'JWT ERROR',
                    message:err.message,
                    error:err
                }
            );
        }

    }
}


export default JWT;