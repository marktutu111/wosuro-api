import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        phone:{
            type:String,
            default:null
        },
        username:{
            type:String,
            default:null
        },
        password:{
            type:String,
            default:null
        },
        email:{
            type:String,
            default:null
        },
        blocked:{
            type:Boolean,
            default:false
        },
        createdAt:{
            type:Date,
            default:Date.now
        }
    }
)


export const dbJwt=mongoose.model('jwtdb',schema);