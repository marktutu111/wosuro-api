

const onToken=async(request,h)=>{
    try {
        const data=request.payload;
        const token: string = request.server.methods.generateJWT(data);
        if (typeof token !== 'string' || token === ''){
            throw Error('Token failed');
        }
        return h.response(
            {
                success:true,
                message:'Token generation successful',
                data:token
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err
            }
        )
    }
}

export default {
    onToken
}