import * as joi from "joi";
import Handler from "../handler/jwt.handler";

export const route =[
    {
        path:'/get',
        method:'POST',
        config:{
            auth:false,
            validate:{
                payload:{
                    email:joi.string().optional(),
                    password:joi.string().optional(),
                    phone:joi.string().optional()
                }
            }
        },
        handler:Handler.onToken
    }
]