import * as jsonwebtoken from "jsonwebtoken";
import { secrete } from "../config/jwt.config";

export const GenerateToken = (data,expiry='1h') => (
    jsonwebtoken.sign(
        data,
        secrete, 
        {
            algorithm: 'HS256', 
            expiresIn: expiry
        }
    )
)