import { SendPush } from "./send-push";
import { route } from "./routes";

const PushPlugin = {
    pkg: { name: 'push', version: '1' },
    register: async (server) => {
        server.method('SendPushNotification', SendPush);
        server.route(route);
    }
}

export default PushPlugin;