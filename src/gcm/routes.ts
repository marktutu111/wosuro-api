import * as joi from "joi";

const sendPush = async (request,h) => {
    try {

        let data = request.payload;
        const response = await request.server.methods.SendPushNotification(data);
        return h.response(
            {
                success: true,
                data: response
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err
            }
        )
    }
}

export const route = [
    {
        path: '/push',
        method: 'POST',
        config: {
            validate: {
                payload: {
                    registration_ids: joi.array().required(),
                    data: joi.object().optional(),
                    collapse_key: joi.string().optional().default('type_a'),
                    notification: joi.object(
                        {
                            body: joi.string().required(),
                            title: joi.string().required()
                        }
                    ),
                }
            }
        },
        handler: sendPush
    }
]