import * as request from "request";
import { push_config } from "./config";


export const SendPush = (data): Promise<any> => new Promise((resolve,reject) => {
    const {KEY,URL}=push_config;
    const headers = {
        'Content-type': 'application/json;charset=UTF-8',
        'Authorization': `key=${KEY}`
    }

    const options = {
        url: URL,
        headers: headers,
        body:JSON.stringify(data), 
    }

    request.post(options, (err, res, bd) => {
        try {
            if (err) reject(err);
            resolve(JSON.parse(bd));
        } catch (err) {
            reject(err);    
        }
    });
})