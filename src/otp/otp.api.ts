import { OTP } from "./db/otp.db";
import { route } from "./route/otp.route";
import Handler from "./handle/otp.handle";

const plugin = {
    pkg: {
        name: 'otp',
        version: '1'
    },
    register: async (server) => {
        server.method('dbOtp', OTP);
        server.method('validateOTP',Handler.validateOTP)
        server.route(route);
    }
}

export default plugin;