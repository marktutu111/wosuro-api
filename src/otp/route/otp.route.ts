import * as joi from "joi";
import Handler from "../handle/otp.handle";

export const route = [
    {
        path: '/sendotp',
        method: 'POST',
        config: {
            auth:false,
            validate: {
                payload: {
                    phoneNumber: joi.string().required()
                }
            }
        },
        handler: Handler.sendOtp
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getOtp
    }
]