import * as mongoose from "mongoose";
import * as moment from "moment";

const schema = new mongoose.Schema(
    {
        phoneNumber: {
            type:String,
            required: true
        },
        otp: {
            type:String,
            max:6
        },
        createdAt: {
            default:Date.now,
            type:Date
        },
        expires: {
            type:Date,
            default: moment().add(30, 'minutes')
        },
        updatedAt: {
            default:Date.now,
            type:Date
        }
    }
);

export const OTP = mongoose.model('otp', schema);