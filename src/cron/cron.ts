var CronJob = require('cron').CronJob;
import Pending from "./pending-payments";

const cron ={
    pkg:{
        name:'cron',
        version:'1'
    },
    register:async(server)=>{
        const job = new CronJob('* * * * *',function(){
            Pending(server).catch(err=>server.methods.dbErrors.create(
                {
                    id:'CRON ERROR',
                    message:err.message,
                    error:err
                }
            ))
        });
        job.start();
    }
}

export default cron;