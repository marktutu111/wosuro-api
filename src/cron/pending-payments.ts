import request = require("request");


const pending=async(server)=>{
    try {
        const transactions=await server.methods.dbTransactions.find(
            {
                status:'pending'
            }
        );
        transactions.forEach(async ({transaction})=>{
            try {
                const response=await server.methods.WigalSendPayment(
                    {
                        type:'STATUS',
                        payload:transaction['transactionid']
                    }
                );
                return server.methods.callbackHandler(
                    server,
                    response
                )
            } catch (err) {
                throw Error(err.message);
            }
        })
    } catch (err) {
        server.methods.dbErrors.create(
            {
                id:'CRON ERROR',
                message:err.message,
                error:err
            }
        )
    }
}


export default pending;