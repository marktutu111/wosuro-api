'use strict';

const Hapi = require('hapi');
import * as Good from "good";
import * as mongoose from "mongoose";

import { goodConfig } from "./config/good.config";
import { _server } from "./config/server.config";
import * as path from "path";

// ROUTES //
import { Db_config } from "./config/database.config";

// plugins
import JWT from "./hapi-auth-jwt/jwt";
import OTP from "./otp/otp.api";
import App from "./app/app.api";
import Wigal from "./wigal";
import RouteMobile from "./route-mobile";
import Uploader from "./uploader/uploader.api";
import Cron from "./cron/cron";
import FileManager from "./files-manager/files.api";
import PushNotifications from "./gcm";

// nes
import RT from "./realtime/rt";



// Start the server
async function start() {
    try {

        // SERVER CONFIGURATION //
        const server = Hapi.server({
            port: _server.port,
            host: _server.host,
            routes: { 
                cors: { 
                    origin: ['*']
                },
                files: {
                    relativeTo:path.resolve('uploads')
                }
            }
        });

        await server.register(require('inert'));

        // REGISTER SERVER PLUGINS //
        await server.register([
            {
                plugin: Good,
                options: goodConfig
            },
            {
                plugin:OTP,
                routes:{
                    prefix:'/wosuro/otp'
                }
            },
            {
                plugin:FileManager,
                routes:{
                    prefix:'/wosuro/file'
                }
            },
            RT,
            PushNotifications,
            Wigal,
            RouteMobile,
            JWT,
            Uploader,
            Cron,
            App
        ])

        // CONNECT TO DATABASE
        await mongoose.connect(Db_config.URL, { useNewUrlParser: true });
        
        // START SERVER //
        await server.start();

        console.log('Server running at:', server.info.uri);


    } catch (err) {
        console.log(err);
        process.exit(1);
    }
};

start();