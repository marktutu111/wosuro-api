
const api={
    pkg:{
        name:'file-reader',
        version:'1'
    },
    register:async(server)=>{
        server.route(
            [
                {
                    path:'/{path}',
                    method:'GET',
                    config:{
                        auth:false
                    },
                    handler: function (request,h) {
                        return h.file(request.params.path);
                    }
                }
            ]
        )
    }
}


export default api;