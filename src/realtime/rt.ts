import * as Nes from "nes";
import Handler from "./handler/realtime.handler";

const realtime = {
    pkg: {
        name: 'rt-api',
        version: '1'
    },
    register:async(server) => {
        try {

            server.register(
                {
                    plugin:Nes,
                    options: {
                        onConnection:null,
                        onDisconnection:Handler.OnDisconnectionHandler,
                        onMessage: Handler.OnMessageHandler
                    }
                }
            );

            server.subscription(`/onbet`);
            
        } catch (err) {
            
        }
    }
}


export default realtime;