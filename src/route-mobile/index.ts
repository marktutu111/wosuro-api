import { SendRequest } from "./process/send-request";

const routeMobile = {
    pkg: {
        name: 'route-mobile',
        version: '1'
    },
    register: async (server) => {
        server.method('sendSMS', SendRequest);
    }
}

export default routeMobile;