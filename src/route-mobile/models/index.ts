
export interface RouteMobileSmsPayload {
    destination: string;
    source: string;
    message: string;
}