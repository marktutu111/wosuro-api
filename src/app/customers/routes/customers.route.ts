import * as joi from "joi";
import Handler from "../handler/customers.handler";


export const routes = [
    {
        path: '/new',
        method: 'POST',
        config: {
            auth:false,
            validate: {
                payload: {
                    phone: joi.string().required().length(10).error(new Error('Phone number must be 10 digits')),
                    otp: joi.string().required().error(new Error('OTP is not valid')),
                    name:joi.string().required(),
                    password:joi.string().required()
                }
            }
        },
        handler: Handler.NewCustomer
    },
    {
        path: '/update',
        method: ['POST','PUT'],
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    data: joi.object()
                }
            },
            handler: Handler.UpdateCustomer
        }
    },
    {
        path: '/get',
        method: 'GET',
        handler: Handler.getCustomers
    },
    {
        path: '/get/{id}',
        method: 'GET',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            },
            handler: Handler.getCustomerById
        }
    },
    {
        path: '/search',
        method: 'GET',
        config: {
            validate: {
                query: {
                    text: joi.string().required()
                }
            },
            handler: Handler.SearchCustomer
        }
    },
    {
        path: '/filter',
        method: 'POST',
        handler: Handler.FilterCustomer
    },
    {
        path: '/delete/{id}',
        method: 'DELETE',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            },
            handler: Handler.DeleteCustomer
        }
    },
    {
        path: '/login',
        method: 'POST',
        config: {
            auth: false,
            validate: {
                payload: {
                    phone: joi.string().required(),
                    password:joi.string().required(),
                    token:joi.string().optional().allow(
                        [
                            null,
                            ''
                        ]
                    )
                }
            },
            handler: Handler.LoginHandle
        }
    },
    {
        path:'/resetpassword',
        method:'POST',
        config:{
            auth:false,
            validate:{
                payload:{
                    phone:joi.string().required(),
                    password:joi.string().required(),
                    otp:joi.string().required()
                }
            }
        },
        handler:Handler.ResetPasssword
    }
]