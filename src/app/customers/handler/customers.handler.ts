import * as mongoose from "mongoose";

const NewCustomer = async (request,h) => {
    try {

        const {
            phone,
            otp,
            name,
            password
        } = request.payload;
        const verified = await request.server.methods.validateOTP(
            {
                phoneNumber: phone,
                otp: otp
            }
        );

        if (!verified)throw Error('Invalid OTP');
        const found = await request.server.methods.dbCustomers.findOne(
            {
                phone:phone
            }
        );

        if (found && found['_id']) {
            return h.response(
                {
                    success: false,
                    message: 'Phone number has been used already',
                    data: null
                }
            )
        }

        const results = await request.server.methods.dbCustomers.create({
            name:name,
            phone:phone,
            password:password
        });
        return h.response(
            {
                success: true,
                message: 'Customer Account Created Succesfully. Please login to continue',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}


const ResetPasssword=async(request,h)=>{
    try {
        const {otp,password,phone}=request.payload;
        const verified=await request.server.methods.validateOTP(
            {
                phoneNumber: phone,
                otp: otp
            }
        );
        if (!verified)throw Error(
            'Invalid OTP'
        );
        const result=await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                phone:phone
            },
            {
                $set:{
                    password:password
                }
            }
        );
        if(!result || !result._id){
            throw Error(
                'Password reset failed phone number does not exist.'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const UpdateCustomer=async(request,h)=>{
    try {
        let {
            id,
            data
        } = request.payload;
        let results = await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set: {
                    ...data,
                    updatedAt: Date.now()
                }
            },
            {
                new: true
            }
        );
        return h.response(
            {
                success: true,
                message: 'Customer updated',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}




const DeleteCustomer = async (request,h) => {
    try {

        let {
            id
        } = request.params;
        let results = await request.server.methods.dbCustomers.findOneAndRemove(
            {
                _id: id
            }
        );
        return h.response(
            {
                success: false,
                message: 'Customer deleted',
                data: results
            }
        )
        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}



const getCustomers = async (request,h) => {
    try {

        const results = await request.server.methods.dbCustomers.find(
            {}
        ).limit(1000).sort({ createdAt: -1 });
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}



const getCustomerById = async (request,h) => {
    try {

        let {
            id
        } = request.params;
        const key=mongoose.Types.ObjectId.isValid(id) ? '_id':'phone';
        let results = await request.server.methods.dbCustomers.findOne(
            {
                [key]:id
            }
        );
        if(!results || !results._id){
            throw Error('customer not found!');
        }
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
}



const FilterCustomer = async (request,h) => {
    try {

        const filter=request.payload;
        const results = await request.server.methods.dbCustomers.find(
            filter
        ).limit(1000).sort({ createdAt: -1 });
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}


const SearchCustomer = async (request,h) => {
    try {

        let text = request.query;
        const results = await request.server.methods.dbCustomers.find(
            { $text: { $search : text } }
        ).sort({ createdAt: -1 }).limit(1000);
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}





const LoginHandle = async (request, h) => {
    try {

        const {
            phone,
            password,
            token
        } = request.payload;
        const customer = await request.server.methods.dbCustomers.findOne(
            {
                phone:phone,
                password:password
            }
        );

        if(!customer || !customer._id){
            throw Error('Customer not found with number or password is not valid, Please contact customer care');
        }

        if(typeof token === 'string' && token !== ''){
            await customer.update(
                {
                    token:token,
                    updatedAt:Date.now()
                }
            )
        }

        const _token:string = request.server.methods.generateJWT(
            {
                phone:customer['phone'],
                password:customer['password']
            }
        );
        return h.response(
            {
                success: true,
                message: 'success',
                data: customer,
                token:_token
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        );

    }
}

export default {
    NewCustomer,
    UpdateCustomer,
    DeleteCustomer,
    FilterCustomer,
    SearchCustomer,
    getCustomerById,
    getCustomers,
    LoginHandle,
    ResetPasssword
}