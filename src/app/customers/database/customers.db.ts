import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        name:{
            index:'text',
            type:String,
            default:null
        },
        password:{
            type:String,
            default:null
        },
        phone:{
            type:String,
            default:null
        },
        blocked:{
            type:Boolean,
            default:false
        },
        totalBets:{
            type:Number,
            default:0
        },
        totalWins:{
            type:Number,
            default:0
        },
        last_seen:{
            type:Date,
            default:null
        },
        token:{
            type:String,
            default:null
        },
        deposit:{
            type:Number,
            default:0
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export const model=mongoose.model('customers',schema);