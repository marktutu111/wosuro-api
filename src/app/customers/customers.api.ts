import { model } from "./database/customers.db";
import { routes } from "./routes/customers.route";

const api={
    pkg:{
        name:'customers-api',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbCustomers',model);
        server.route(routes);
    }
}

export default api;