

const getErrors = async (request,h) => {
    try {
        const results = await request.server.methods.dbErrors.find(
            {}
        ).limit(1000).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

export default {
    getErrors
}