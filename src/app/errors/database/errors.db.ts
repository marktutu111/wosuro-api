import * as mongoose from "mongoose";

const schema = new mongoose.Schema(
    {
        id: String,
        message: String,
        error: String,
        createdAt: {
            type: Date,
            default: Date.now
        }
    }
)

export const errors = mongoose.model('logs',schema);