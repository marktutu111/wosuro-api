import * as joi from "joi";
import Handler from "../handler/errors.handler";

export const routes = [
    {
        path: '/get',
        method: 'GET',
        handler: Handler.getErrors
    }
]