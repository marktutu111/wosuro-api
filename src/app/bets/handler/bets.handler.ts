import * as mongoose from "mongoose";

const createBet=async(request,h)=>{
    const data=request.payload;
    try {
        const verified = await request.server.methods.validateOTP(
            {
                phoneNumber:data['accountNumber'],
                otp:data['otp']
            }
        );
        if (!verified)throw Error('Invalid OTP');
        const game=await request.server.methods.dbGamePlay.findOneAndUpdate(
            {
                _id:data['gameId'],
                productId:data['productId'],
                active:true,
                gameover:false
            },
            {
                $set:{
                    updatedAt:Date.now()
                },
            },{new:true}
        );

        if(!game || !game._id){
            throw Error('Sorry you are too slow your slots have been taken, please try again.');
        }
        
        game.reservations.forEach(slot=>{
            if(data['slotNumber'].indexOf(slot)>-1){
                throw Error('Sorry, It looks like some of your slots have been taken already. Please choose a different slot.');
            }
        });

        const bet=await request.server.methods.dbBets.findOne(
            {
                gameId:game._id,
                slotNumber:{
                    $in:data['slotNumber']
                }
            }
        );

        if(bet && bet._id){
            throw Error(
                'Sorry, It looks like some of your slots have been taken already. Please choose a different slot.'
            )
        }

        await game.update(
            {
                $push: {
                    reservations: {
                        $each:data['slotNumber']
                    } 
                }
            }
        )

        const transaction=await request.server.methods.dbTransactions.create(
            {
                transaction_type:'PLAY',
                customerId:data['customerId'],
                gameId:game['_id'],
                accountNumber:data['accountNumber'],
                accountIssuer:data['accountIssuer'],
                accountType:data['accountType'],
                amount:data['amount'],
                slotNumber:data['slotNumber']
            }
        );

        switch (data['accountType']) {
            case 'momo':
                const response=await request.server.methods.sendPayment(
                    request,
                    transaction
                );
                return h.response(response);
            default:
                throw Error('Dear customer, We are sorry we cannot process your payment type.');
        }
    } catch (err) {
        request.server.methods.dbGamePlay.findOneAndUpdate(
            {
                _id:data['gameId'],
                productId:data['productId']
            },
            {
                $pullAll: {
                    reservations:data['slotNumber']
                }
            }
        ).catch(err=>null);
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const spin=async(request,h)=>{
    try {
        const {
            gameId,
            customerId,
            rewardId,
            score
        }=request.payload;
        const game=await request.server.methods.dbGamePlay.findOne(
            {
                _id:gameId,
                gameover:false,
                active:true
            }
        );
        if(!game || !game._id){
            throw Error(
                'Game not found'
            )
        };
        const deposit=await request.server.methods.dbDeposits.findOneAndUpdate(
            {
                customerId:customerId,
                points:{
                    $gte:game[
                        'points_per_spin'
                    ]
                }
            },
            {
                $inc:{
                    points:-game[
                        'points_per_spin'
                    ]
                }
            },{new:true}
        );
        if(!deposit || !deposit._id){
            throw Error(
                'Sorry we couldnt process your score, you don\nt have enough points, kindly topup.'
            )
        }
        const spin=await request.server.methods.dbBets.findOneAndUpdate(
            {
                gameId:gameId,
                customerId:customerId,
                rewardId:rewardId
            },
            {
                $set:{
                    gameId:gameId,
                    customerId:customerId,
                    rewardId:rewardId,
                    updatedAt:Date.now()
                },
                $inc:{
                    score:score,
                    numberOfSpins:1
                }
            },{ upsert:true,new:true }
        ).populate('customerId').populate('rewardId').populate('gameId');
        if(!spin || !spin._id){
            throw Error(
                'Sorry we could process your request, Kindy try again'
            )
        }
        request.server.methods.processWheel(
            request,
            spin
        ).catch(err=>null);
        return h.response(
            {
                success:true,
                message:'success',
                data:spin,
                points:deposit['points']
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}




const getBetsById=async(request,h)=>{
    try {
        const {id}=request.params;
        const bet=await request.server.methods.dbBets.find(
            {
                gameId:id
            }
        ).populate('productId').populate('customerId').populate('gameId').populate('rewardId')
        return h.response(
            {
                success:true,
                message:'success',
                data:bet
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getWinners=async(request,h)=>{
    try {
        const bets=await request.server.methods.dbBets.find(
            {
                won:true
            }
        ).sort({ createdAt:-1 }).populate('productId').populate('customerId').populate('gameId').populate('rewardId');
        return h.response(
            {
                success:true,
                message:'success',
                data:bets
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const getBet=async(request,h)=>{
    try {
        const {id}=request.params;
        const bet=await request.server.methods.dbBets.findOne(
            {
                $or:[
                    {
                        _id:id
                    },
                    {
                        gameId:id
                    }
                ]
            }
        ).populate('productId').populate('customerId').populate('gameId').populate('rewardId')
        if(!bet || !bet._id){
            throw Error(
                'Bet Not Found!'
            );
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:bet
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getBets=async(request,h)=>{
    try {
        const results=await request.server.methods.dbBets.find({})
            .sort({createdAt:-1})
            .limit(1000)
            .populate('productId')
            .populate('customerId')
            .populate('rewardId')
            .populate(
                {
                    path:'gameId',
                    select:'-winningSlot'
                }
            )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getCustomerBets=async(request,h)=>{
    try {
        const {id}=request.params;
        const results=await request.server.methods.dbBets.find(
            {
                customerId:id
            }
        ).sort({updatedAt:-1})
            .limit(200)
            .populate('productId')
            .populate('customerId')
            .populate('rewardId')
            .populate(
                {
                    path:'gameId',
                    select:'-winningSlot'
                }
            )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const DeleteBet = async (request,h) => {
    try {

        let {
            id
        } = request.params;
        let results = await request.server.methods.dbBets.findOneAndRemove(
            {
                _id: id
            }
        );
        if(!results || !results._id){
            throw Error(
                'Delete failed'
            )
        }
        return h.response(
            {
                success: false,
                message: 'Bet deleted',
                data: results
            }
        )
        
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}


const filter=async(request,h)=>{
    try {
        let _filter={};
        const filter=request.payload;
        Object.keys(filter).forEach(key=>{
            if(key && filter[key]){
                _filter[key]=filter[key];   
            }
        })
        const results=await request.server.methods.dbBets.find(_filter).sort({createdAt:-1})
            .limit(1000)
            .populate('productId')
            .populate('customerId')
            .populate('gameId')
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    createBet,
    getBets,
    getBet,
    filter,
    getCustomerBets,
    DeleteBet,
    getBetsById,
    getWinners,
    spin
}