import * as mongoose from "mongoose";

const handler=async(server,{_id})=>{
    try {

        const transaction=await server.methods.dbTransactions.findOne(
            {
                _id:_id,
                status:'paid',
                transaction_type:'PLAY'
            }
        ).populate('customerId').populate('gameId');
        if(!transaction || !transaction._id){
            throw Error(
                'Transaction not found'
            )
        }

        const {status}=transaction;
        const betcode=`${transaction['accountNumber'].split("").reverse().join("").substring(0,4)}${transaction['slotNumber'].join('')}`;
        const customer=transaction['customerId'];
        const gameId:string=transaction['gameId']['_id'];
        const productId:string=transaction.gameId['productId'];

        transaction.update(
            {
                betcode:betcode
            }
        ).catch(err=>null);
        let message:string='';
        if (status==='paid'){
            const game=await server.methods.dbGamePlay.findOne(
                {
                    _id:gameId,
                    active:true,
                    gameover:false,
                }
            );
            if(game && game._id){
                await server.methods.dbBets.findOneAndUpdate(
                    {
                        customerId:customer['_id'],
                        gameId:gameId
                    },
                    {
                        $set:{
                            gameId:gameId,
                            customerId:customer['_id'],
                            productId:game['productId'],
                            productCode:game['productCode'],
                            updatedAt:Date.now(),
                            createdAt:Date.now()
                        },
                        $inc:{
                            amount:transaction['amount']
                        },
                        $push: {
                            slotNumber: {
                                $each:transaction['slotNumber']
                            } 
                        }
                    },
                    { upsert:true,new:true}
                );
                if(game.reservations.length === game.betcount){
                    await game.update(
                        {
                            active:false,
                            gameover:true,
                            updatedAt:Date.now(),
                            gameEndDate:Date.now()
                        }
                    )
                    server.methods.processResults(
                        server,
                        game._id
                    )
                }
                message='Congratulations '+ customer['name']+'!'
                    + '\n'
                    + 'Your slot has been reserved, '
                    + 'your transaction code is '+ betcode + '. '
                    + 'You will be notified when you win or loose.'
                    + '\n'
                    + 'Hwenadi';

                server.publish('/onbet',transaction);

            }
        }

        if (status==='failed'){
            server.methods.dbGamePlay.findOneAndUpdate(
                {
                    _id:gameId
                },
                {
                    $set:{
                        updatedAt:Date.now()
                    },
                    $pullAll: {
                        reservations:transaction['slotNumber']
                    }
                }
            ).catch(err=>null);
            message='Dear '+ customer['name']+','
                    + '\n'
                    + 'Your transaction was not successful, '
                    + 'please try again or call customer care if problem persist.'
                    + '\n'
                    + 'Hwenadi';

        }

        // send sms
        if(status !== 'pending'){
            server.methods.sendSMS(
                {
                    message:message,
                    destination:customer['phone']
                }
            ).catch(err=>null);
        }

        // send push notification
        // to all participating customer
        if(status==='paid'){
            const bets=await server.methods.dbBets.find(
                {
                    gameId:mongoose.Types.ObjectId(gameId)
                }
            ).populate('customerId');
            const product:any=await server.methods.dbProducts.findOne(
                {
                    _id:productId
                }
            );

            const ids:string[]=[];
            let productName:string='';

            if(product && product._id){
                productName=product.name;
            }

            bets.forEach(bet=>{
                const customer=bet.customerId;
                ids.push(customer.token);
            });
            if(ids.length > 0){
                server.methods.SendPushNotification(
                    {
                        registration_ids:ids,
                        data:{},
                        collapse_key:'type_a',
                        notification:{
                            body:`${customer.name} has purchased a slot for your item ${productName}`,
                            title: 'Slot Purchased'
                        }
                    }
                ).catch(err=>null);
            }
        }
        return {
            success:true,
            message:'callback received'
        }
    } catch (err) {
        console.log(
            err
        )
    }
}


export default handler;