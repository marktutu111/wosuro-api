

const cal=async(request,bet)=>{
    try {
        const {
            gameId,
            rewardId,
            score
        }=bet;
        const game=await request.server.methods.dbGamePlay.findOneAndUpdate(
            {
                _id:gameId,
                reward:rewardId,
                game_type:'RW'
            },
            {
                $set:{
                    updatedAt:Date.now()
                },
                $inc:{
                    totalPoints:score,
                }
            },{new:true}
        );
        if(!game || !game._id){
            throw Error(
                'Game not found'
            )
        };
        const {
            maxPoints,
            totalPoints
        }=game;

        const gameOver:boolean=totalPoints>maxPoints;
        const active:boolean=!gameOver;
        let highestScore=0;

        if(gameOver){
            const bets:any[]=await request.server.methods.dbBets.find(
                {
                    gameId:game[
                        '_id'
                    ]
                }
            ).populate('customerId');
            highestScore=bets.map(spin=>spin['score']).reduce((a,b)=>Math.max(a,b));
            const bet=await request.server.methods.dbBets.findOneAndUpdate(
                {
                    gameId:game['_id'],
                    score:highestScore
                },
                {
                    $set:{
                        won:true
                    }
                },{new:true}
            ).populate('customerId').populate('rewardId');
            const {customerId,rewardId}=bet;
            let message='Congratulations '+ customerId['name'] + '!'
                + '\n'
                + 'you are the winner for ' + rewardId['name'] + ', '
                + 'you will receive an sms with reward details.'
                + '\n'
                + 'Tonadi';
            request.server.methods.sendSMS(
                {
                    message:message,
                    destination:customerId['phone']
                }
            ).catch(err=>null);
            request.server.methods.SendPushNotification(
                {
                    to:customerId.token,
                    data:{},
                    collapse_key:'type_a',
                    notification:{
                        body:`Congratulations ${customerId.name}!, you have won ${rewardId['name']}, you will receive an sms with reward details.`,
                        title: 'You Won!'
                    }
                }
            ).catch(err=>null);
            bets.forEach(spin=>{
                if(spin['score'] !== highestScore){
                    const customer=spin['customerId'];
                    let message='Sorry '+ customer['name'] + '!'
                        + '\n'
                        + 'you didn\'t win your contest. '
                        + 'check the app for more details'
                        + '\n'
                        + 'Tonadi';
                    request.server.methods.sendSMS(
                        {
                            message:message,
                            destination:customer['phone']
                        }
                    ).catch(err=>null);
                }
            });
            game.update(
                {
                    gameEndDate:Date.now()
                }
            ).catch(err=>null);
        }
        await game.update(
            {
                gameover:gameOver,
                active:active,
                highestScore:highestScore
            }
        );
        return gameOver && request.server.methods.createSpinGame(
            request,
            game['reward']
        );
    } catch (err) {
        console.log(err);
        throw Error(
            err.message
        )
    }
}

export default cal;