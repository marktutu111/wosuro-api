import * as mongoose from "mongoose";


const processResults=async(server,gameId:string)=>{
    try {

        const game=await server.methods.dbGamePlay.findOne(
            {
                _id:gameId,
                gameover:true
            }
        ).populate('productId');
        if(!game || !game._id)throw Error('Game is still in progress!');
        const bets=await server.methods.dbBets.find(
            {
                gameId:mongoose.Types.ObjectId(game['_id'])
            }
        ).populate('customerId');
        bets.forEach(bet=>{
            const customer=bet['customerId'];
            if(bet['slotNumber'].indexOf(game['winningSlot'])>-1){
                bet.update(
                    {
                        won:true
                    }
                ).catch(err=>null);
                server.methods.dbWins.findOneAndUpdate(
                    {
                        gameId:game['_id'],
                        customerId:customer._id
                    },
                    {
                        $set:{
                            gameId:game['_id'],
                            customerId:customer._id,
                            productId:game['productId']['_id'],
                            productCode:game['productCode'],
                            createdAt:Date.now()
                        }
                    },{ upsert:true }
                ).catch(err=>null);

                let message='Congratulations '+ customer['name'] + '!'
                + '\n'
                + 'Your lucky slot number ' + '(' + game['winningSlot'] + ') '
                + 'was selected as the Winning Slot. '
                + 'your item code is '+ game['productCode'] + '. '
                + 'kindly visit our office to Recieve your Item.'
                + '\n'
                + 'Tonadi';
                server.methods.sendSMS(
                    {
                        message:message,
                        destination:customer['phone']
                    }
                ).catch(err=>null);
                server.methods.SendPushNotification(
                    {
                        to:customer.token,
                        data:{},
                        collapse_key:'type_a',
                        notification:{
                            body:`Congratulations ${customer.name}!, Your lucky slot number (${game['winningSlot']}) was selected as the Winning Slot.`,
                            title: 'You Won!'
                        }
                    }
                ).catch(err=>null);
            }else{
                let message='Dear '+ customer['name'] + '!'
                + '\n'
                + 'We are sorry! You did not win your bet for item '
                + game['productCode'] + '. The winning number is '
                + game['winningSlot'] + '.'
                + 'Better luck next time.'
                + '\n'
                + 'Tonadi';
                server.methods.sendSMS(
                    {
                        message:message,
                        destination:customer['phone']
                    }
                ).catch(err=>null);
                server.methods.SendPushNotification(
                    {
                        to:customer.token,
                        data:{},
                        collapse_key:'type_a',
                        notification:{
                            body:`We are sorry! You did not win your bet for item ${game['productCode']}, Check your history for details.`,
                            title: 'You Lost!'
                        }
                    }
                ).catch(err=>null);
            }
        });

        return server.methods.createGame(
            server,
            game['productId']
        );
    } catch (err) {
        server.methods.dbErrors.create(
            {
                id:'RESULTS ERROR',
                message:err.message,
                error:err
            }
        );
        return err.message;
    }
}


export default processResults;