import * as mongoose from "mongoose";
import * as randomise from "randomatic";

const schema=new mongoose.Schema(
    {
        gameId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'gameplay',
            required:true
        },
        customerId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'customers',
            required:true
        },
        productId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'products'
        },
        rewardId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'rewards'
        },
        numberOfSpins:{
            type:Number,
            default:0
        },
        score:{
            type:Number,
            default:0
        },
        slotNumber:{
            type:[Number],
            default:[]
        },
        productCode:{
            type:String,
            default:null
        },
        amount:{
            type:Number,
            default:0
        },
        won:{
            type:Boolean,
            default:false
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export const bets=mongoose.model('bets',schema);