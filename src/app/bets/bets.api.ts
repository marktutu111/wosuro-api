import { bets } from "./database/bets.db";
import { routes } from "./routes/bets.route";
import callbackHandler from "./handler/bet.payment.handler";
import processResults from "./handler/betresults.handler";
import processWheel from "./handler/bet.spin.results";

const api={
    pkg:{
        name:'bets',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbBets',bets);
        server.method('callbackHandler',callbackHandler);
        server.method('processResults',processResults);
        server.method('processWheel',processWheel);
        server.route(routes)
    }
}

export default api;