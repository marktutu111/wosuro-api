import * as joi from "joi";
import Handler from "../handler/bets.handler";

export const routes=[
    {
        path:'/get',
        method:'GET',
        handler:Handler.getBets
    },
    {
        path:'/create',
        method:'POST',
        config:{
            validate:{
               payload:{
                    gameId:joi.string().required(),
                    customerId:joi.string().required(),
                    productId:joi.string().required(),
                    amount:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ).required(),
                    slotNumber:joi.array().allow(
                        joi.number()
                    ).required().min(1),
                    productCode:joi.string().optional(),
                    accountNumber:joi.string().required(),
                    accountIssuer:joi.string().required(),
                    accountType:joi.optional().default('momo'),
                    otp:joi.string().required()
               } 
            }
        },
        handler:Handler.createBet
    },
    {
        path:'/spin/create',
        method:'POST',
        config:{
            validate:{
                payload:{
                    gameId:joi.string().required(),
                    customerId:joi.string().required(),
                    rewardId:joi.string().required(),
                    score:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ).required()
                }
            }
        },
        handler:Handler.spin
    },
    {
        path:'/get/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.getBet
    },
    {
        path:'/winners/get',
        method:'GET',
        handler:Handler.getWinners
    },
    {
        path:'/all/get/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.getBetsById
    },
    {
        path:'/customer/get/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.getCustomerBets
    },
    {
        path:'/delete/{id}',
        method:'DELETE',
        config: {
            validate: {
                params: {
                    id: joi.string().required()
                }
            }
        },
        handler: Handler.DeleteBet
    }
]