import schema from "./database/companies.db";
import route from "./routes/companies.route";


const api={
    pkg:{
        name:'companies',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbCompanies',schema);
        server.route(route);
    }
}

export default api;