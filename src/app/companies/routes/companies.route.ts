import * as joi from "joi";
import Handler from "../handler/companies.handler";

const routes=[
    {
        path:'/add',
        method:'POST',
        config:{
            payload: {
                parse:true,
                maxBytes: 1000 * 1000 * 5, // 10 Mb
                output: 'data'
            },
            validate:{
                payload:{
                    companyName:joi.string().required(),
                    companyPhone:joi.string().required(),
                    companyLogo:joi.string().optional()
                }
            }
        },
        handler:Handler.addcompany
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getcompanies
    },
    {
        path:'/update',
        method:'PUT',
        config:{
            validate:{
                payload:{
                    id:joi.string().required(),
                    data:joi.object().required()
                }
            }
        },
        handler:Handler.update
    },
    {
        path:'/delete/{id}',
        method:'DELETE',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.deletecompany
    },
    {
        path:'/login',
        method:'POST',
        config:{
            validate:{
                payload:{
                    companyCode:joi.string().required(),
                    companyPhone:joi.string().required()
                }
            }
        },
        handler:Handler.login
    }
]


export default routes;