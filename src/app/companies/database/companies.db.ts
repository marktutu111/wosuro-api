import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        companyName:{
            type:String,
            uppercase:true,
            default:null
        },
        companyCode:{
            type:String,
            default:null,
            maxlength:5
        },
        companyPhone:{
            type:String,
            default:null
        },
        companyLogo:{
            type:String,
            default:null
        },
        blocked:{
            type:Boolean,
            default:false
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export default mongoose.model('companies',schema);