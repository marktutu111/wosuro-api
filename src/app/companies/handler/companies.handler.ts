const addcompany=async(request,h)=>{
    try {
        const data=request.payload;
        let count:number=await request.server.methods.dbCompanies.count({});
        count=count+1;
        const companyCode=`${'00000'.substring(count.toString().length,5)}${count.toString()}`;
        const results=await request.server.methods.dbCompanies.create(
            {
                ...data,
                companyCode:companyCode
            }
        );
        if(!results || !results._id){
            throw Error('Company could not be saved');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getcompanies=async(request,h)=>{
    try {
        const results=await request.server.methods.dbCompanies.find(
            {}
        ).sort({createdAt:-1}).limit(1000);
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const update=async(request,h)=>{
    try {
        const {id,data}=request.payload;
        const response=await request.server.methods.dbCompanies.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:data
            },
            {
                new:true
            }
        )
        if(!response || !response._id){
            throw Error('Update failes');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:response
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const deletecompany=async(request,h)=>{
    try {
        const {id}=request.params;
        const results=await request.server.methods.dbCompanies.findOneAndRemove(
            {
                _id:id
            }
        )
        if(!results || !results._id){
            throw Error('Delete failed')
        }
        return h.response(
            {
                success:true,
                message:'company deleted',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const login=async(request,h)=>{
    try {
        const {companyPhone,companyCode}=request.payload;
        const company=await request.server.methods.dbCompanies.findOne(
            {
                companyPhone:companyPhone,
                companyCode:companyCode,
                blocked:false
            }
        )
        if(!company || !company._id){
            throw Error('Account not found or maybe blocked');
        }
        const token: string = request.server.methods.generateJWT(company.toObject());
        return h.response(
            {
                success:true,
                message:'success',
                data:company,
                token:token
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

export default {
    addcompany,
    getcompanies,
    update,
    deletecompany,
    login
}