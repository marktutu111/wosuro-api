import customers from "./customers/customers.api";
import Products from "./products/products.api";
import GamePlay from "./gameplay/gameplay.api";
import Bets from "./bets/bets.api";
import Wins from "./wins/wins.api";
import Transactions from "./transactions/transactions.api";
import Companies from "./companies/companies.api";
import Admin from "./admin/admin.api";
import Errors from "./errors/errors.api";
import Rewards from "./rewards/rewards.api";
import Deposits from "./deposits/deposit.api";

// Utils
import Utils from "./utils/utils.module";

const api={
    pkg:{
        name:'app',
        version:'1'
    },
    register:async(server)=>{
        server.register(
            [
                Utils,
                {
                    plugin:customers,
                    routes:{
                        prefix:'/wosuro/customers'
                    }
                },
                {
                    plugin:Products,
                    routes:{
                        prefix:'/wosuro/products'
                    }
                },
                {
                    plugin:GamePlay,
                    routes:{
                        prefix:'/wosuro/gameplay'
                    }
                },
                {
                    plugin:Companies,
                    routes:{
                        prefix:'/wosuro/companies'
                    }
                },
                {
                    plugin:Bets,
                    routes:{
                        prefix:'/wosuro/bets'
                    }
                },
                {
                    plugin:Wins,
                    routes:{
                        prefix:'/wosuro/wins'
                    }
                },
                {
                    plugin:Deposits,
                    routes:{
                        prefix:'/wosuro/deposits'
                    }
                },
                {
                    plugin:Transactions,
                    routes:{
                        prefix:'/wosuro/transactions'
                    }
                },
                {
                    plugin:Admin,
                    routes:{
                        prefix:'/wosuro/admin'
                    }
                },
                {
                    plugin:Errors,
                    routes:{
                        prefix:'/wosuro/logs'
                    }
                },
                {
                    plugin:Rewards,
                    routes:{
                        prefix:'/wosuro/rewards'
                    }
                }
            ]
        )
    }
}

export default api;