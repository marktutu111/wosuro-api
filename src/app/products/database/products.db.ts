import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        name:{
            type:String,
            index:'text',
            default:null
        },
        quantity:{
            type:Number,
            default:0
        },
        remaining:{
            type:Number,
            default:0
        },
        itemPrice:{
            type:Number,
            default:0
        },
        companyName:{
            type:String,
            uppercase:true,
            default:null
        },
        companyCode:{
            type:String,
            default:null,
            maxlength:5
        },
        betPrice:{
            type:Number,
            default:0
        },
        active:{
            type:Boolean,
            default:true
        },
        image:{
            type:String,
            default:null
        },
        betcount:{
            type:Number,
            default:30
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export const products=mongoose.model('products',schema);