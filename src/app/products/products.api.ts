import { products } from "./database/products.db";
import { route } from "./routes/products.route";

const api={
    pkg:{
        name:'product-api',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbProducts',products);
        server.route(route);
    }
}


export default api;