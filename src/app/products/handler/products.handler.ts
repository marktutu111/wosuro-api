

const addProduct=async(request,h)=>{
    try {
        const data=request.payload;
        const company=await request.server.methods.dbCompanies.findOne(
            {
                companyName:data['companyName'],
                companyCode:data['companyCode'],
            }
        )
        if(!company || !company._id){
            throw Error('Company not found in our system');
        }
        const results=await request.server.methods.dbProducts.create(
            {
                ...data,
                remaining:data['quantity']
            }
        );
        const path=await request.server.methods.Uploader(
            {
                image:data['image'],
                id:results._id
            }
        );
        await results.update(
            {
                image:path
            }
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const deleteProduct=async(request,h)=>{
    try {
        let {
            id
        } = request.params;
        let results = await request.server.methods.dbProducts.findOneAndRemove(
            {
                _id: id
            }
        );
        return h.response(
            {
                success: false,
                message: 'Product deleted',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}

const getProducts = async (request,h) => {
    try {
        const results = await request.server.methods.dbProducts.find(
            {}
        ).limit(1000).sort({ createdAt: -1 });
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}

const getproduct = async (request,h) => {
    try {

        let {
            id
        } = request.params;
        let results = await request.server.methods.dbProducts.findOne(
            {
                _id:id
            }
        );
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
    }
};


const updateImage=async(request,h)=>{
    try {
        const {id,image}=request.payload;
        const product=await request.server.methods.dbProducts.findOne(
            {
                _id:id
            }
        )
        if(!product || !product._id){
            throw Error(
                'Product not found'
            )
        };
        const path=await request.server.methods.Uploader(
            {
                image:image,
                id:product._id
            }
        );
        await product.update(
            {
                image:path
            }
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:product
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const updateProduct = async (request,h) => {
    try {

        let {
            id,
            data
        } = request.payload;
        let results = await request.server.methods.dbProducts.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set: {
                    ...data,
                    updatedAt: Date.now()
                }
            },
            {
                new: true
            }
        );
        return h.response(
            {
                success:true,
                message:'product updated',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}

const search=async(request,h)=>{
    try {
        const {item}=request.query;
        const results = await request.server.methods.dbProducts.find(
            { $text: { $search : item } }
        ).sort({ createdAt: -1 }).limit(10);
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    addProduct,
    deleteProduct,
    updateProduct,
    getProducts,
    getproduct,
    search,
    updateImage
}
