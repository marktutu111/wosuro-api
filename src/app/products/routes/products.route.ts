import * as joi from "joi";
import Handler from "../handler/products.handler";

export const route=[
    {
        path:'/new',
        method:'POST',
        config:{
            payload: {
                parse:true,
                maxBytes: 1000 * 1000 * 5, // 10 Mb
                output: 'data'
            },
            validate:{
                payload:{
                    name:joi.string().required(),
                    quantity:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ),
                    itemPrice:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ),
                    betPrice:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ),
                    image:joi.string().required(),
                    companyName:joi.string().required(),
                    companyCode:joi.string().required(),
                    betcount:joi.string().optional().default(30)
                }
            }
        },
        handler:Handler.addProduct
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getProducts
    },
    {
        path:'/get/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.getproduct
    },
    {
        path:'/delete/{id}',
        method:'DELETE',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.deleteProduct
    },
    {
        path: '/update',
        method: ['POST','PUT'],
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    data: joi.object()
                }
            },
        },
        handler:Handler.updateProduct
    },
    {
        path: '/image/update',
        method: ['POST','PUT'],
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    image: joi.string().required()
                }
            },
        },
        handler:Handler.updateImage
    },
    {
        path:'/search',
        method:'GET',
        config:{
            validate:{
                query:{
                    item:joi.string().required()
                }
            }
        },
        handler:Handler.search
    }
]