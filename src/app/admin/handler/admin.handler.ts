
const getall=async(request,h)=>{
    try {
        const results=await request.server.methods.dbAdmin.find({});
        return h.response(
            {
                success:false,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const login=async(request,h)=>{
    try {
        const {email,password}=request.payload;
        const admin=await request.server.methods.dbAdmin.findOne(
            {
                email:email,
                password:password,
                blocked:false
            }
        )
        if(!admin || !admin._id){
            throw Error('Account not found!');
        }
        const token: string = request.server.methods.generateJWT(admin.toObject());
        return h.response(
            {
                success:true,
                message:'Login successful',
                data:admin,
                token:token
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const add=async(request,h)=>{
    try {
        const data=request.payload;
        const results=await request.server.methods.dbAdmin.create(data);
        if(!results || !results._id){
            throw Error('Account not created');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    login,
    add,
    getall
}