import * as joi from "joi";
import Handler from "../handler/admin.handler";

const routes=[
    {
        path:'/login',
        method:'POST',
        config:{
            auth:false,
            validate:{
                payload:{
                    email:joi.string().email().required(),
                    password:joi.string().required()
                }
            }
        },
        handler:Handler.login
    },
    {
        path:'/add',
        method:'POST',
        config:{
            auth:false,
            validate:{
                payload:{
                    email:joi.string().required(),
                    password:joi.string().required(),
                    name:joi.string().required()
                }
            }
        },
        handler:Handler.add
    },
    {
        path:'/get',
        method:'GET',
        config:{
            auth:false
        },
        handler:Handler.getall
    }
]


export default routes;