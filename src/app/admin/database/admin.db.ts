import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        name:{
            type:String,
            default:null
        },
        email:{
            type:String,
            default:null
        },
        role:{
            type:String,
            default:'super',
            enum:['super','admin']
        },
        password:{
            type:String,
            default:null
        },
        blocked:{
            type:Boolean,
            default:false
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model('admin',schema);