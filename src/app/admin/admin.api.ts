import { EOPNOTSUPP } from "constants";
import model from "./database/admin.db";
import routes from "./route/admin.route";

const api={
    pkg:{
        name:'admin',
        version:'1'
    },
    register:async server=>{
        server.method('dbAdmin',model);
        server.route(routes);
    }
}

export default api;