import * as mongoose from "mongoose";


const schema=new mongoose.Schema(
    {
        customerId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'customers'
        },
        points:{
            type:Number,
            default:0
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model(
    'deposits',
    schema
)