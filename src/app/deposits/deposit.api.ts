import model from "./database/deposit.db";
import callbackHandler from "./handler/deposit.callback";
import route from "./routes/deposits.route";

const api={
    pkg:{
        name:'deposit',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbDeposits',model);
        server.method('depositHandler',callbackHandler);
        server.route(route);
    }
}

export default api;