import * as joi from "joi";
import Handler from "../handler/deposit.handler";

const routes=[
    {
        path:'/get',
        method:'GET',
        handler:Handler.getdeposits
    },
    {
        path:'/get/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.getdeposit
    },
    {
        path:'/new',
        method:'POST',
        config:{
            validate:{
                payload:{
                    customerId:joi.string().required(),
                    accountNumber:joi.string().required(),
                    accountIssuer:joi.string().required(),
                    accountType:joi.optional().default('momo'),
                    otp:joi.string().required(),
                    amount:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ).required()
                }
            }
        },
        handler:Handler.deposit
    },
    {
        path: '/update',
        method: ['POST','PUT'],
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    data: joi.object()
                }
            },
            handler: Handler.update
        }
    },
]

export default routes;