
const deposit=async(request,h)=>{
    try {
        const data=request.payload;
        const verified = await request.server.methods.validateOTP(
            {
                phoneNumber:data['accountNumber'],
                otp:data['otp']
            }
        );
        if (!verified)throw Error('Invalid OTP');
        const transaction=await request.server.methods.dbTransactions.create(
            {
                transaction_type:'DEPOSIT',
                customerId:data['customerId'],
                accountNumber:data['accountNumber'],
                accountIssuer:data['accountIssuer'],
                accountType:data['accountType'],
                amount:data['amount']
            }
        );
        switch (data['accountType']) {
            case 'momo':
                const response=await request.server.methods.sendPayment(
                    request,
                    transaction
                );
                return h.response(response);
            default:
                throw Error('Dear customer, We are sorry we cannot process your payment type.');
        }
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const getdeposit=async(request,h)=>{
    try {
        const {id}=request.params;
        const results=await request.server.methods.dbDeposits.findOne(
            {
                $or:[
                    {
                        _id:id
                    },
                    {
                        customerId:id
                    }
                ]
            }
        ).populate('customerId');
        if(!results || !results._id){
            throw Error(
                'Deposit not available'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getdeposits=async(request,h)=>{
    try {
        const results=await request.server.methods.dbDeposits.find(
            {}
        ).populate('customerId').sort({createdAt:-1});
        return h.response(
            {
                success:false,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const update=async(request,h)=>{
    try {
        let {
            id,
            data
        } = request.payload;
        let results = await request.server.methods.dbDeposits.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set: {
                    ...data,
                    updatedAt: Date.now()
                }
            },
            {
                new: true
            }
        );
        return h.response(
            {
                success: true,
                message: 'Deposit updated',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}


export default {
    deposit,
    getdeposit,
    getdeposits,
    update
}