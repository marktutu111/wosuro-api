
const deposit=async(server,{_id})=>{
    try {
        const transaction=await server.methods.dbTransactions.findOne(
            {
                _id:_id,
                status:'paid',
                transaction_type:'DEPOSIT'
            }
        ).populate('customerId');
        if(!transaction || !transaction._id){
            throw Error(
                'Transaction not found'
            )
        }
        const{
            customerId,
            amount
        }=transaction;
        const deposit=await server.methods.dbDeposits.findOneAndUpdate(
            {
                customerId:customerId[
                    '_id'
                ]
            },
            {
                $set:{
                    customer:customerId['_id'],
                    updatedAt:Date.now()
                },
                $inc:{
                    points:amount*1000
                }
            },
            { upsert:true,new:true}
        );
        if(!deposit || !deposit._id){
            throw Error(
                'Transaction failed'
            )
        }
        let message='Congratulations '+ customerId['name'] + '!'
            + '\n'
            + 'Your account has been credited with '
            + deposit['points'] + 'points.'
            + '\n'
            + '\n'
            + 'Tonadi';
        server.methods.sendSMS(
            {
                message:message,
                destination:customerId['phone']
            }
        ).catch(err=>null);
        server.methods.SendPushNotification(
            {
                to:customerId.token,
                data:{},
                collapse_key:'type_a',
                notification:{
                    body:`Congratulations ${customerId.name}!, Your account has been credited with ${deposit['points']} points.`,
                    title: 'Deposit Alert'
                }
            }
        ).catch(err=>null);
    } catch (err) {
        console.log(
            err
        );
        
    }
}

export default deposit;