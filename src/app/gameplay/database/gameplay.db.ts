import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";


const schema=new mongoose.Schema(
    {
        reward:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'rewards'
        },
        productId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'products'
        },
        itemPrice:{
            type:Number,
            default:0
        },
        game_type:{
            type:String,
            default:'PD',
            enum:[
                'RW', // rewards game
                'PD' // products
            ]
        },
        highestScore:{
            type:Number,
            default:0
        },
        totalPoints:{
            type:Number,
            default:0
        },
        maxPoints:{
            type:Number,
            default:0
        },
        points_per_spin:{
            type:Number,
            default:0
        },
        spinRewards:{
            type:[String],
            default:[]
        },
        betPrice:{
            type:Number,
            default:0
        },
        betcount:{
            type:Number,
            default:0
        },
        winningSlot:{
            type:Number,
            default:0
        },
        reservations:{
            type:[Number],
            default:[]
        },
        odds:{
            type:[Number],
            default:[]
        },
        active:{
            type:Boolean,
            default:true
        },
        productCode:{
            type:String,
            default:null
        },
        gameover:{
            type:Boolean,
            default:false
        },
        gameEndDate:{
            type:Date,
            default:null
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

schema.plugin(mongoosePaginate);
export const model=mongoose.model('gameplay',schema);