import * as mongoose from "mongoose";
import * as randomise from "randomatic";



const create=async(request,h)=>{
    try {
        const {productId}=request.payload;
        const game=await request.server.methods.createGame(request.server,productId);
        if(!game || !game._id){
            throw Error(
                'Game Error'
            );
        }
        return h.response(
            {
                success:true,
                message:'Game Created!',
                data:game
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};


const createSpin=async(request,h)=>{
    try {
        const {rewardId}=request.payload;
        const game=await request.server.methods.createSpinGame(
            request,
            rewardId
        );
        if(!game || !game._id){
            throw Error(
                'Oops something went wrong'
            );
        }
        return h.response(
            {
                success:true,
                message:'Game Created!',
                data:game
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};





const getWinningSlot=async(request,h)=>{
    try {
        const {id}=request.params;
        if(!mongoose.Types.ObjectId.isValid(id)){
            throw Error('Invalid ID');
        }
        const result=await request.server.methods.dbGamePlay.findOne(
            {
                _id:id,
                gameover:true,
                active:false,
                where:function(){
                    return this.reservations.length===this.betcount
                }
            },
        )
        if(!result || !result._id){
            throw Error('Slot not found!');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:result['winningSlot']
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getGame=async(request,h)=>{
    try {
        const {id}=request.params;
        const key=mongoose.Types.ObjectId.isValid(id) ? '_id':'productCode';
        const game=await request.server.methods.dbGamePlay.findOne(
            {
                [key]:id
            },
            {
                winningSlot:0
            }
        )
        if(!game || !game._id){
            throw Error('Game Not Found!');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:game
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const filterProduct=async(request,h)=>{
    try {
        const {item}=request.query;
        const products = await request.server.methods.dbProducts.find(
            { 
                $text: {
                    $search:item
                }
            },
            {
                _id:1
            }
        ).sort({ createdAt: -1 }).limit(10);
        const results=await request.server.methods.dbGamePlay.find(
            {
                productId:{
                    $in:products
                }
            },
            {
                winningSlot:0
            }
        ).limit(10).populate('productId');
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const deleteGame=async(request,h)=>{
    try {
        const {id}=request.params;
        const game=await request.server.methods.dbGamePlay.findOneAndRemove(
            {
                _id:id
            }
        )
        if(!game || !game._id){
            throw Error('Delete failed');
        }
        return h.response(
            {
                success:false,
                message:'success',
                data:game
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getGames=async(request,h)=>{
    try {
        const results=await request.server.methods.dbGamePlay.find(
            {},{winningSlot:0}
        ).sort({createdAt:-1}).limit(1000).populate('productId');
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};

const getOngoing=async(request,h)=>{
    try {
        const results=await request.server.methods.dbGamePlay.find(
            {
                active:true,
                gameover:false
            },
            {
                winningSlot:0
            }
        ).sort({createdAt:-1}).limit(10).populate('productId');
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const paginate=async(request,h)=>{
    try {
        const {filter={},page,limit}=request.payload;
        const results=await request.server.methods.dbGamePlay.paginate(
            filter,
            {
                select:'-winningSlot',
                populate:['productId','reward'],
                page:page,
                limit:limit,
                collation: {
                    locale: 'en'
                },
                sort:{
                    createdAt:-1
                }
            }
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:results.docs
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const updateGame = async (request,h) => {
    try {
        let {
            id,
            data
        } = request.payload;
        let results = await request.server.methods.dbGamePlay.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set: {
                    ...data,
                    updatedAt: Date.now()
                }
            },
            {
                new: true
            }
        );
        return h.response(
            {
                success: true,
                message: 'Game updated',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}


const processGameResults=async(request,h)=>{
    try {
        const {id}=request.payload;
        const response=await request.server.methods.processResults(
            request.server,
            id
        );
        return h.response(
            response
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

export default {
    create,
    getGame,
    getGames,
    getOngoing,
    paginate,
    deleteGame,
    filterProduct,
    updateGame,
    processGameResults,
    getWinningSlot,
    createSpin
}