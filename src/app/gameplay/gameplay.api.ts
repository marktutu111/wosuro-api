import { model } from "./database/gameplay.db";
import { routes } from "./routes/gameplay.route";


const api={
    pkg:{
        name:'gameplay',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbGamePlay',model);
        server.route(routes);
    }
}


export default api;