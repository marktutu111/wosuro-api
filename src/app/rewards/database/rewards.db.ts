import * as mongoose from "mongoose";


const schema=new mongoose.Schema(
    {
        name:{
            type:String,
            default:null
        },
        price:{
            type:Number,
            default:0
        },
        label:{
            type:String,
            default:null
        },
        wheelPoints:{
            type:[String],
            default:[]
        },
        winningPoints:{
            type:Number,
            default:0
        },
        points_per_spin:{
            type:Number,
            default:0
        },
        wheelBg:{
            type:String,
            default:null
        },
        active:{
            type:Boolean,
            default:true
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export default mongoose.model('rewards',schema);