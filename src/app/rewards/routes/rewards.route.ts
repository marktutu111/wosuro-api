import * as joi from "joi";
import Handler from "../handler/reward.handler";

const route=[
    {
        path:'/add',
        method:'POST',
        config:{
            payload:{
                parse:true,
                maxBytes: 1000 * 1000 * 5, // 10 Mb
                output: 'data'
            },
            validate:{
                payload:{
                    name:joi.string().required(),
                    price:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ),
                    label:joi.string(),
                    wheelPoints:joi.array(),
                    winningPoints:joi.alternatives(
                        joi.string(),
                        joi.number()
                    ),
                    wheelBg:joi.string()
                }
            }
        },
        handler:Handler.addRewards
    },
    {
        path:'/delete/{id}',
        method:'DELETE',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.deleteReward
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getRewards
    },
    {
        path: '/update',
        method: ['POST','PUT'],
        config: {
            validate: {
                payload: {
                    id: joi.string().required(),
                    data: joi.object()
                }
            },
        },
        handler:Handler.updateReward
    },
]


export default route;