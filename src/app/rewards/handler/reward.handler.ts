

const addRewards=async(request,h)=>{
    try {
        const data=request.payload;
        const result=await request.server.methods.dbRewards.create(
            {
                ...data,wheelBg:'',label:''
            }
        );
        if(!result || !result._id){
            throw Error(
                'Something went wrong'
            )
        };
        const [
            label,
            wheelBg
        ]=await Promise.all(
            [
                request.server.methods.Uploader(
                    {
                        image:data['label'],
                        id:`lbl${result._id}`
                    }
                ),
                request.server.methods.Uploader(
                    {
                        image:data['wheelBg'],
                        id:`wb${result._id}`
                    }
                ),
            ]
        );
        await result.update(
            {
                label:label,
                wheelBg:wheelBg
            }
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err){
        return h.response(
            {
                success:false,
                message:err.message,
            }
        )
    }
}


const deleteReward=async(request,h)=>{
    try {
        let {
            id
        } = request.params;
        let results = await request.server.methods.dbRewards.findOneAndRemove(
            {
                _id: id
            }
        );
        return h.response(
            {
                success: false,
                message: 'Reward deleted',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}


const getRewards=async(request,h)=>{
    try {
        const results = await request.server.methods.dbRewards.find(
            {}
        ).limit(1000).sort({ createdAt: -1 });
        return h.response(
            {
                success: true,
                message: 'success',
                data: results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}


const updateReward = async (request,h) => {
    try {

        let {
            id,
            data
        } = request.payload;
        let results = await request.server.methods.dbRewards.findOneAndUpdate(
            {
                _id: id
            },
            {
                $set: {
                    ...data,
                    updatedAt: Date.now()
                }
            },
            {
                new: true
            }
        );
        return h.response(
            {
                success:true,
                message:'reward updated',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        )
        
    }
}


export default {
    addRewards,
    deleteReward,
    getRewards,
    updateReward
};