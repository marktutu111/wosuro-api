import db from "./database/rewards.db";
import route from "./routes/rewards.route";

const api={
    pkg:{
        name:'rewards-api',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbRewards',db);
        server.route(route);
    }
}


export default api;