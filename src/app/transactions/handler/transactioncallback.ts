import * as mongoose from "mongoose";

const callback=async(server,payload)=>{
    try {

        let status: 'paid' | 'failed' | 'pending' = 'pending';
        let referenceId:string = payload['clienttransid'];
        if(!mongoose.Types.ObjectId.isValid(referenceId)){
            throw Error(
                'Invalid Reference Id'
            );
        }
        switch (payload.status) {
            case 'PROGRESS':
                status='pending';
                break;
            case 'PAID':
                status='paid';
                break;
            default:
                status='failed';
                break;
        }
        const transaction=await server.methods.dbTransactions.findOneAndUpdate(
            {
                _id:referenceId
            },
            {
                $set:{
                    transaction:payload,
                    status:status
                }
            },{new:true}
        );
        if(!transaction || !transaction._id){
            throw Error(
                'Transaction not found!'
            );
        }
        switch (transaction.transaction_type) {
            case 'PLAY':
                server.methods.callbackHandler(
                    server,
                    transaction
                )
                break;
            case 'DEPOSIT':
                server.methods.depositHandler(
                    server,
                    transaction
                )
                break;
            default:
                break;
        }
        return {
            success:true,
            message:'callback received'
        }
    } catch (err) {
        return {
            success:true,
            message:'callback received'
        }
    }
}


export default callback;