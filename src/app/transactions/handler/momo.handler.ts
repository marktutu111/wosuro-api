

const momoPay=async(request,transaction)=>{
    try {
        const {
            _id,
            amount,
            accountNumber,
            accountIssuer
        }=transaction;
        const paymentData:any={
            "amount":amount,
            "clienttransid":_id,
            "nickname": "Tonadi",
            "paymentoption":accountIssuer,
            "walletnumber":accountNumber
        }
        const response = await request.server.methods.WigalSendPayment(
            {
                type:'DEBIT',
                payload:paymentData
            }
        );
        let status:string='';
        let message:string='';
        switch (response.status) {
            case 'OK':
                status='pending';
                message=`Your request is received, you will receive a prompt on your phone to authorize payment for GHS${transaction['amount']}. Reservation has been made for your selected coupons and will be deleted if payment is not made.`;
                break;
            default:
                status='failed';
                message='Sorry we could not process your request, Please contact customer care.';
                break;
        }
        transaction.update(
            {
                transaction:response,
                status:status
            }
        ).catch(err=>null);
        return {
            success:true,
            message:message,
            data:transaction
        }
    } catch (err) {
        return {
            success:false,
            message:err.message
        }
    }
}


export default momoPay;