

const getTransactions=async(request,h)=>{
    try {
        const results=await request.server.methods.dbTransactions.find(
            {}
        ).limit(1000).sort({createdAt:-1})
            .populate('customerId')
            .populate('betId')
            .populate(
                {
                    path:'gameId',
                    select:'-winningSlot'
                }
            )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getTransaction=async(request,h)=>{
    try {
        const {id}=request.params;
        const win=await request.server.methods.dbTransactions.findOne(
            {
                _id:id
            }
        ).populate('customerId').populate(
            {
                path:'gameId',
                select:'-winningSlot'
            }
        )
        if(!win || !win._id){
            throw Error('Transaction Not Found!');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:win
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const purchaseMomoCallback=async(request,h)=>{
    try {
        const payload=request.payload;
        const response=await request.server.methods.transactionCallback(
            request.server,
            payload
        )
        return h.response(response)
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}





export default {
    getTransactions,
    getTransaction,
    purchaseMomoCallback
}