import * as joi from "joi";
import Handler from "../handler/transactions.handler";

export const route=[
    {
        path:'/get',
        method:'GET',
        handler:Handler.getTransactions
    },
    {
        path:'/get/{id}',
        method:'GET',
        config:{
            validate:{
                params:{
                    id:joi.string().required()
                }
            }
        },
        handler:Handler.getTransaction
    },
    {
        path:'/callback',
        method:'POST',
        config:{
            auth:false,
            validate:{
                payload:joi.object()
            }
        },
        handler:Handler.purchaseMomoCallback
    },
]