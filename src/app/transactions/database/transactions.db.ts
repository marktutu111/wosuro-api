import * as mongoose from "mongoose";


const schema=new mongoose.Schema(
    {
        customerId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'customers'
        },
        betId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bets'
        },
        gameId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'gameplay'
        },
        transaction_type:{
            type:String,
            default:'PLAY',
            enum:[
                'DEPOSIT',
                'PLAY'
            ]
        },
        accountNumber:{
            type:String,
            default:null
        },
        accountIssuer:{
            type:String,
            default:null
        },
        accountType:{
            type:String,
            default:null
        },
        amount:{
            type:Number,
            default:0
        },
        betcode:{
            type:String,
            default:null
        },
        slotNumber:{
            type:[Number],
            default:[]
        },
        status:{
            enum:['pending','failed','paid',null],
            type:String,
            default:null
        },
        transaction:{
            type:Object,
            default:null
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export const transaction=mongoose.model('transactions',schema);