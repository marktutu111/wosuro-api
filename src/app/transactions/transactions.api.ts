import { transaction } from "./database/transactions.db";
import momoPay from "./handler/momo.handler";
import transactionCallback from "./handler/transactioncallback";
import { route } from "./route/transactions.route";

const api={
    pkg:{
        name:'transaction',
        version:'1'
    },
    register:async server=>{
        server.method('dbTransactions',transaction);
        server.method('transactionCallback',transactionCallback);
        server.method('sendPayment',momoPay);
        server.route(route);
    }
}


export default api;