import createGame from "./creategame";
import createSpin from "./createspingame";
import { formatePhonenumber } from "./formate-phone-number";

const utils={
    pkg:{
        name:'utils',
        version:'1'
    },
    register:async(server)=>{
        server.method('createGame',createGame);
        server.method('createSpinGame',createSpin);
        server.method('formatePhonenumber',formatePhonenumber);
    }
}


export default utils;