import * as randomise from "randomatic";


const createGame=async(request,rewardId:string)=>{
    try {
        const reward=await request.server.methods.dbRewards.findOne(
            {
                _id:rewardId,
                active:true
            }
        );
        if(!reward || !reward._id){
            throw Error(
                'Rewward is not active'
            )
        }
        const code:string=`${new Date().getTime().toString().substring(0,5)}${randomise('0',2)}`;
        let rdata={
            game_type:'RW',
            reward:reward._id,
            spinRewards:[],
            productCode:code,
            maxPoints:reward['winningPoints'],
            points_per_spin:reward['points_per_spin'] || 1000
        };
        const running=await request.server.metthods.dbGamePlay.findOne(
            {
                gameover:false,
                active:true,
                reward:reward[
                    '_id'
                ],
            }
        );
        if(running && running._id){
            throw Error(
                'Game is already running'
            )
        }
        const game=await request.server.methods.dbGamePlay.create(
            rdata
        );
        return game;
    } catch (err) {
        console.log(err);
        throw Error(
            err
        )
    }
}


export default  createGame;