import * as randomise from "randomatic";

const createNewGame=async(server,id:string)=>{
    try {
        let i:number=1;
        let odds:number[]=[];
        const game=await server.methods.dbGamePlay.findOne(
            {
                productId:id,
                active:true,
                gameover:false
            }
        );
        if(game && game._id){
            throw Error(
                'Game is still ongoing for this Product'
            );
        }
        const product=await server.methods.dbProducts.findOneAndUpdate(
            {
                _id:id,
                remaining:{
                    $gt:0
                }
            },
            {
                $set:{
                    updatedAt:Date.now()
                },
                $inc:{
                    remaining:-1
                }
            }
        )
        if(!product || !product._id){
            return false;
        }
        const shuffle=()=>{
            let j, x, i;
            for (i = odds.length - 1; i > 0; i--) {
                j = Math.floor(Math.random() * (i + 1));
                x = odds[i];
                odds[i] = odds[j];
                odds[j] = x;
            }
        };
        while (i<=product.betcount){
            odds.push(i);
            i++;
        }
        const winning_slot:number=odds[Math.floor(Math.random()*odds.length)];
        const productCode:string=`${product.companyCode}${new Date().getTime().toString().substring(0,5)}${randomise('0',2)}`;
        const data={
            game_type:'PD',
            productId:product._id,
            itemPrice:product.itemPrice,
            betPrice:product.betPrice,
            betcount:product.betcount,
            winningSlot:winning_slot,
            odds:odds,
            productCode:productCode
        }
        return await server.methods.dbGamePlay.create(
            data
        );
    } catch (err) {
        return err.message;
    }
}

export default createNewGame;