import { wins } from "./database/wins.db";
import { routes } from "./routes/wins.route";

const api={
    pkg:{
        name:'wins',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbWins',wins);
        server.route(routes);
    }
}

export default api;