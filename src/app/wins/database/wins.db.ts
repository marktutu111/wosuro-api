import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        productId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'products'
        },
        customerId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'customers'
        },
        gameId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'gameplay'
        },
        productCode:{
            type:String,
            default:null
        },
        received:{
            type:Boolean,
            default:false
        },
        dateReceived:{
            type:Date,
            default:null
        },
        receipient_name:{
            type:String,
            default:null
        },
        receipient_phone:{
            type:String,
            default:null
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export const wins=mongoose.model('wins',schema);