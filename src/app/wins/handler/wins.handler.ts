
const getWin=async(request,h)=>{
    try {
        const {id}=request.params;
        const win=await request.server.methods.dbWins.findOne(
            {
                _id:id
            }
        ).populate('productId').populate('customerId').populate('gameId');
        if(!win || !win._id){
            throw Error('Game Not Found!');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:win
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getWins=async(request,h)=>{
    try {
        const results=await request.server.methods.dbWins.find({})
            .sort({createdAt:-1})
            .limit(1000)
            .populate('productId')
            .populate('customerId')
            .populate(
                {
                    path:'gameId',
                    select:'-winningSlot'
                }
            )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const collectItem=async(request,h)=>{
    try {
        const {
            otp,
            productCode,
            customerPhone,
            receipient_name,
            receipient_phone
        }=request.payload;
        const verified = await request.server.methods.validateOTP(
            {
                phoneNumber:customerPhone,
                otp:otp
            }
        );
        if (!verified)throw Error('Invalid OTP');
        const game=await request.server.methods.dbGamePlay.findOne(
            {
                productCode:productCode,
                gameover:true,
                active:false
            }
        );
        if(!game || !game._id){
            throw Error('Item cannot be processed now, Game is still ongoing, Please contact customer care.');
        }
        const results=await request.server.methods.dbWins.findOneAndUpdate(
            {
                productCode:productCode
            },
            {
                $set:{
                    received:true,
                    dateReceived:Date.now(),
                    receipient_name:receipient_name,
                    receipient_phone:receipient_phone
                }
            },{new:true}
        )
        if(!results || !results._id){
            throw Error('Sorry!, We cannot process delivery at the moment');
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const filter=async(request,h)=>{
    try {
        let _filter={};
        const filter=request.payload;
        Object.keys(filter).forEach(key=>{
            if(key && filter[key]){
                _filter[key]=filter[key];   
            }
        })
        const results=await request.server.methods.dbWins.find(_filter).sort({createdAt:-1})
            .limit(1000)
            .populate('productId')
            .populate('customerId')
            .populate(
                {
                    path:'gameId',
                    select:'-winningSlot'
                }
            )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const addone=async(request,h)=>{
    try {
        const response=await request.server.methods.dbWins.create(request.payload);
        return h.response(
            {
                success:true,
                message:'success',
                data:response
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    getWin,
    getWins,
    filter,
    collectItem,
    addone
}