import sendRequest from "./process/wigal.process";
const wigal = {
    pkg: {
        name: 'wigal-api',
        version: '1'
    },
    register: async (server) => {
        server.method('WigalSendPayment',sendRequest);
    }
}


export default wigal;