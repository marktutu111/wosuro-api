import * as request from "request";

const sendRequest = (
    {
        type,
        payload
    }
) => new Promise((resolve,reject)=>{

    let url: string = "";
    let method=null;
    let headers=null;
    switch (type) {
        case 'DEBIT':
            url = "https://api.reddeonline.com/v1/receive";
            method='post';
            headers = {
                'Content-type': 'application/json;charset=UTF-8',
                'apikey': '5usChwEs/sn2IMhuP93gG30H+u46o4jDRRG5l1d9Z6o=',
            }
            break;
        case 'CREDIT':
            url = "https://api.reddeonline.com/v1/cashout";
            method='post';
            headers = {
                'Content-type': 'application/json;charset=UTF-8',
                'apikey': '5usChwEs/sn2IMhuP93gG30H+u46o4jDRRG5l1d9Z6o='
            }
            break;
        default:
            url=`https://api.reddeonline.com/v1/status/${payload}`;
            method='get';
            headers = {
                'Content-type': 'application/json;charset=UTF-8',
                'apikey': '5usChwEs/sn2IMhuP93gG30H+u46o4jDRRG5l1d9Z6o=',
                'appid':'55'
            }
            break;
    }

    const data = Object.assign(payload, { appid: '55', nickname: 'Tonadi' });
    const options = {
        url: url,
        headers: headers,
        body: JSON.stringify(data)
    }

    request[method](options, (err, res, bd) => {
        try {
            if (err) reject(err);
            resolve(JSON.parse(bd));
        } catch (err) {
            reject(err);    
        }
    });
    
})


export default sendRequest;