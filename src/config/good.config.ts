export const goodConfig = {
    ops: {
        interval: 1000
    },
    reporters: {
        myConsoleReporter: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ log: '*', response: '*' }]
        }, {
            module: 'good-console'
        }, 'stdout'],
        // myFileReporter: [{
        //     module: 'good-squeeze',
        //     name: 'Squeeze',
        //     args: [{ ops: '*' }]
        // }, {
        //     module: 'good-squeeze',
        //     name: 'SafeJson'
        // }, {
        //     module: 'good-file',
        //     args: ['logs/server-logs']
        // }],
        // myHTTPReporter: [{
        //     module: 'good-squeeze',
        //     name: 'Squeeze',
        //     args: [{ error: '*' }]
        // }, {
        //     module: 'good-http',
        //     args: ['http://localhost:3000/logs', {
        //         wreck: {
        //             headers: { 'x-api-key': 12345 }
        //         }
        //     }]
        // }]
    }
}