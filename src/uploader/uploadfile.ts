import * as fs from "fs";
import * as path from "path";

const uploader=({image,id})=>new Promise((resolve, reject)=>{
    const _path=path.resolve(`uploads/${id}.jpg`);
    var base64Data = image.split(',')[1];
    fs.writeFile(_path,base64Data,'base64',(err)=>{
        if (err)return reject(err);
        resolve(`http://172.105.22.95:8020/wosuro/file/${id}.jpg`);
    })
});


const downloader=(path:string)=>new Promise((resolve, reject) => {
    fs.readFile(path,(err,data)=>{
        if (err)return reject();
        resolve(data);
    })
});

const deleteFile=(path:string)=>new Promise((resolve, reject) => {
    fs.unlink(path,(err)=>{
        if (err)return reject(false);
        resolve(true);
    })
});


export { uploader,downloader,deleteFile }